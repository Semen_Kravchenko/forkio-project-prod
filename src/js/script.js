const iconWrapper = document.querySelector('.burger');
const icon = document.querySelector('.burger__icon');
const menu = document.querySelector('.menu');
const menuWrap = document.querySelector('.navbar__menu-wrapper');

function togglerClass() {
    icon.classList.toggle('burger__icon--active');
    menuWrap.classList.toggle('vis');
    menu.classList.toggle('menu--active');
}

function removeClass() {
    icon.classList.remove('burger__icon--active');
    menuWrap.classList.remove('vis');
    menu.classList.remove('menu--active');
}

iconWrapper.addEventListener('click', togglerClass);
window.addEventListener('resize', removeClass);
window.addEventListener('scroll', removeClass);
document.body.addEventListener('click', function(evt) {
    if(evt.target !== menuWrap) {
        return;
    } else {
        removeClass();
    }
})
